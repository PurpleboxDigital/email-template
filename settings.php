<?php
global $wpdb, $email_template_options;
ksort($email_template_options);
?>

<div class="wrap">
    <div class="email_template_settings">
        <h1 class="wp-heading-inline">Email Template</h1>
        <h2>Template Variables</h2>
        <p>Site URL: {{site_url}}</p>
        <p>Site Name: {{site_name}}</p>
        <p>Message*: {{message}}</p>
        <p><i>*Required variable, without it the email will have no content</i></p>
        <form method="post" action="options.php">
            <?php settings_fields( 'email_template_options_group' ); ?>
            <table>
                <?php foreach($email_template_options as $key => $value) {
                    $label = str_replace("email_template_", "", $key);
                    $label = str_replace("_", " ", $label);
                    $label = ucwords($label);
                    ?>
                    <tr valign="top">
                        <th scope="row">
                            <?php echo $label;?>
                        </th>
                    </tr>
                    <tr valign="top">
                        <td>
                            <hr>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <?php echo wp_editor( get_option($key), $key, array('textarea_name' => $key, 'media_buttons' => true)  ); ?>
                        </td>
                    </tr>
                <?php }?>
            </table>
            <?php  submit_button(); ?>
        </form>

    </div>
</div>

<style>
    .email_template_settings h2 {
        margin-top: 20px;
    }
    .email_template_settings table {
        width: 100%;
    }
    .email_template_settings th,
    .email_template_settings tr {
        text-align: left;
        width: 50%;
    }
    .email_template_settings tr {
        text-align: left;
        width: 50%;
    }
    .email_template_settings textarea {
        width: 100%;
        height: 400px;
    }
    .email_template_settings a {
        text-decoration: none;
    }
</style>