<?php
/**
 * Plugin Name: Email Template
 * Description: Adds custom template to emails.
 * Version: 1.0.1
 * Author: Purplebox Digital
 */

/**
 * Custom Email Content
 */
function email_template_handle_wp_mail($atts)
{
    global $email_template_options;

    // site url
    $site_url = home_url();

    // site_name
    $site_name = get_bloginfo('name');

    // message
    $message = $atts['message'];

    // template
    $template.= "<!DOCTYPE html>";
    $template.= "<html>";
    $template.= "<head>";
    $template.= "<meta charset=\"UTF-8\">";
    $template.= "<title></title>";
    $template.= "</head>";
    $template.= "<body>";
    $template.= get_option('email_template_general_template');
    $template.= "</body>";
    $template.= "</html>";

    // variables to replace in template
    $content = str_replace("{{message}}", $message, $template);
    $content = str_replace("{{site_url}}", $site_url, $content);
    $content = str_replace("{{site_name}}", $site_name, $content);

    // message content
    $atts['message'] = $content;

    return ($atts);
}
add_filter('wp_mail','email_template_handle_wp_mail');

/**
 * Set Emails To HTML
 */
function email_template_set_content_type()
{
    return "text/html";
}
add_filter( 'wp_mail_content_type','email_template_set_content_type' );

/**
 * Format Retrieve Password Link
 */
function email_template_retrieve_password_message( $message )
{
    $message = str_replace('<','',$message);
    $message = str_replace('>','',$message);
    $message = str_replace("\n",'<br>',$message);

    // make links clickable
    $message = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1">$1</a>', $message);

    return $message;
}
add_filter( 'retrieve_password_message', 'email_template_retrieve_password_message', 10, 1 );

/**
 * Options
 */
global $email_template_options;

$email_template_options = array (
    "email_template_general_template" => "",
);

/**
 * Options Page
 */
function email_template_admin_menu_item ()
{
    add_options_page (
        'Email Template',
        'Email Template',
        'manage_options',
        'email-template-settings.php',
        'email_template_options_page'
    );
}
add_action( 'admin_menu', 'email_template_admin_menu_item' );

/**
 * Options Page Content
 */
function email_template_options_page ()
{
    include("settings.php");
}

/**
 * Option Register Settings (Form Fields)
 */
function email_template_register_settings ()
{
    global $email_template_options;

    foreach ($email_template_options as $key => $value)
    {
        add_option($key, $value);
        register_setting('email_template_options_group', $key, 'email_template_callback');
    }
}
add_action( 'admin_init', 'email_template_register_settings' );

/**
 * Add Settings Link To Plugin Page
 */
function email_template_plugin_page_settings_link( $links ) {
    $links[] = '<a href="' . admin_url( 'options-general.php?page=email-template-settings.php' ) . '">' . __('Settings') . '</a>';
    return $links;
}
add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'email_template_plugin_page_settings_link');